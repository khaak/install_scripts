#!/bin/bash

# Обновляем весь софт
yum update
# Иногда еще приходится
rpm -Uvh http://dl.fedoraproject.org/pub/epel/6/x86_64/epel-release-6-8.noarch.rpm
# Устанавливаем часть необходимого софта
yum install wget postfix nano lsof htop lshw iotop -y
# Качаем и выполняем скрипт-установщик
wget http://repos.1c-bitrix.ru/yum/bitrix-env.sh
bash bitrix-env.sh
# долго курим
# Запрашиваем доменное имя (нужно для работы почты)
echo -e "Enter domain name" && read dom_ain 
# Удаляем sendmail
rpm -e --nodeps sendmail
# Обновляем алиасы программ после замены почтовика
newaliases
# Заменяем путь до почтовика в настройках PHP
cp /etc/php.d/bitrixenv.ini /etc/php.d/zz_custom.ini
sed -i 's/msmtp/\/usr\/sbin\/sendmail/g' /etc/php.d/zz_custom.ini
#if [ "$dom_ain" = "" ]; then dom_ain="undef.ru"; fi
# Меняем настройки почтовика (как раз для этого мы вводили домен)
echo -ne "\ninet_protocols=ipv4\nmyhostname = mail."$dom_ain"\nmydomain = "$dom_ain"\nmasquerade_domains = "$dom_ain"\n" >> /etc/postfix/main.cf
# Включаем CURL, устанавлисаем почтовик в автозагрузку и перезапускаем postfix
mv -f /etc/php.d/20-curl.ini.disabled /etc/php.d/20-curl.ini && chkconfig postfix on && service postfix restart
# Смена часового пояса
/bin/cp -f /usr/share/zoneinfo/Europe/Moscow /etc/localtime
# Обновление git
yum install -y curl-devel expat-devel gettext-devel openssl-devel zlib-devel gcc perl-ExtUtils perl-devel
cd /usr/src
gitlatestver=`curl -s https://www.kernel.org/pub/software/scm/git/ | egrep "(git-[[:digit:]].*tar\.gz)" | tail -1 | grep -o -P '(?<=\>).*(?=\.tar.gz<)'`
wget https://www.kernel.org/pub/software/scm/git/$gitlatestver.tar.gz
tar -xvf $gitlatestver.tar.gz
cd $gitlatestver
make prefix=/usr/local/git all
make prefix=/usr/local/git install
# Правка путей
echo "export PATH=/usr/local/git/bin:$PATH" >> /etc/bashrc
source /etc/bashrc
# Рестарт сервисов
service httpd restart && service mysqld restart && service nginx restart
# Запуск меню окружения. При первом запуске меняем пароль пользователя bitrix
bash /root/menu.sh
