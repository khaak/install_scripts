#!/bin/bash

#add repo
rpm --import http://repo.zabbix.com/RPM-GPG-KEY-ZABBIX
echo -ne "[zabbix]\nname=Zabbix Official Repository - \$basearch\nbaseurl=http://repo.zabbix.com/zabbix/2.4/rhel/6/\$basearch/\nenabled=1\ngpgcheck=1\ngpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-ZABBIX\n\n[zabbix-non-supported]\nname=Zabbix Official Repository non-supported - \$basearch\nbaseurl=http://repo.zabbix.com/non-supported/rhel/6/\$basearch/\nenabled=1\ngpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-ZABBIX\ngpgcheck=1\n" > /etc/yum.repos.d/zabbix.repo

#install zabbix
yum -y install zabbix-agent

#configuring zabbix
sed -i 's/Include=\/etc\/zabbix\/zabbix_agentd.d\//Include=\/etc\/zabbix\/zabbix_agentd.d\/*.conf/' /etc/zabbix/zabbix_agentd.conf
rm -f /etc/zabbix/zabbix_agentd.d/*
git clone https://khaak@bitbucket.org/khaak/zabbix_agentd.d.git /etc/zabbix/zabbix_agentd.d/
echo -ne "PidFile=/var/run/zabbix/zabbix_agentd.pid\nLogFile=/var/log/zabbix/zabbix_agentd.log\nLogFileSize=0\nServer=88.198.18.214\nHostname=`hostname`\nEnableRemoteCommands=1\n" > /etc/zabbix/zabbix_agentd.d/zz_custom.conf

#open port
cp /etc/sysconfig/iptables /etc/sysconfig/iptables.orig.`date +%s`
sed -i 's/^COMMIT/-A INPUT -s 88.198.18.214 -p tcp -m tcp --dport 10050 -j ACCEPT\nCOMMIT/' /etc/sysconfig/iptables

#reload services
service iptables reload
service zabbix-agent restart

#add to autorun
chkconfig zabbix-agent on
